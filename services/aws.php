<?php
   require '../vendor/autoload.php';   
   
   use Aws\S3\S3Client;
   use Aws\Exception\AwsException;
   
   $sdk = new Aws\Sdk([
      'profile' => 'test',
      'region' => 'us-east-2',
      'version' => 'latest',
   ]);

   $s3Client = $sdk->createS3();
   
?>
