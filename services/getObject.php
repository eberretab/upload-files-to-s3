<?php
    require 'aws.php';
    try{
        $result = $s3Client->getObject([
            'Bucket'=> 'test-bucket-eber',
            'Key' => $_POST['key'],
        ]);
    } catch (S3Exception $e) {
        echo $e->getMessage();
        exit();
    }

    $getFile = $result->toArray();
    $filename = '../temp_storage/'.$_POST['key'];
    
    // Store file in server temporaly
    file_put_contents($filename,$getFile['Body']);

    if(file_exists($filename)) {
        //Define header information
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: 0");
        header('Content-Disposition: attachment; filename="'.basename($filename).'"');
        header('Content-Length: ' . filesize($filename));
        header('Pragma: public');

        //Clear system output buffer
        flush();

        //Read the size of the file
        readfile($filename);

        //Delete file temporaly from server
        unlink($filename);
    }else{
        echo "File does not exist.";
    }
    
?>